import org.jetbrains.annotations.Contract;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.*;

@RunWith(Parameterized.class)
public class CalculatorTest {

    private Calculator calculator;
    private int n1;
    private int n2;
    private int expectedResult;

    public CalculatorTest(int n1, int n2, int expectedResult) {
        this.calculator = new Calculator();
        this.n1 = n1;
        this.n2 = n2;
        this.expectedResult = expectedResult;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
                { 2, 5 ,10}, {  5, 8 ,40}
        });
    }


    @Test
    public void multiplyTest() {
        int actualMultiply = calculator.multiply(n1, n2);
        assertEquals(actualMultiply,expectedResult);
    }

    @Test
    public void sumTest(){
        assertEquals(Calculator.sum(2,2),4);
    }

    @Test
    public void diffTest(){
        assertEquals(Calculator.diff(3,2),1);
    }

    @Test(expected=java.lang.ArithmeticException.class)
    public void divideTest(){
        Calculator.divide(3,0);
    }

}
